from django.apps import AppConfig


class CurtisConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'curtis'
